/** \page
!!! LevenshteinSerial
 * 
 * This example illustrates the use of the Levenshtein algorithm in the serial approach.
 */


#include  <stdio.h> 
#include  "vector"
#include  "cstring"
#include "tools/tools-text.hpp"
#include "tools/cpu-time.hpp"

#pragma vector always
#pragma vector align  
#pragma ivdep


/*!
 *  This functor represents the Levenshtein algorithm
 */

template<class T>
T prepare_field_v3( const T &s11,int what_world_to_compare)
{                
         const T delim("|||");
         return tools::boostsplit<T>(s11, delim)[0];
}

template<class T>
unsigned int levenshtein_distance_v2( const T &s1, const T & s2) {
	const size_t len1 = s1.size(), len2 = s2.size();
	//__attribute__((aligned(16))) std::vector<unsigned int> col(len2+1) ; 
        //__attribute__((aligned(16))) std::vector<unsigned int> prevCol(len2+1);
	std::vector<unsigned int> col(len2+1) ; 
        std::vector<unsigned int> prevCol(len2+1);

	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
//        #pragma simd noassert
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
                #pragma simd noassert
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
								prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
		col.swap(prevCol);
	}
	return prevCol[len2];
}



struct levenshtein_distance_operator_v2 {
  double operator()( const std::string &a,  const std::string &b) {
     std::string str1 = prepare_field_v3<std::string>(a,-1);
     std::string str2 = prepare_field_v3<std::string>(b,-1);

     double distance = levenshtein_distance_v2<std::string>(str1, str2);  // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance

     if (distance == 0 ) return  distance;
     double minlen = (double) std::min(str1.size(), str2.size());
     return       std::min(1.,distance/minlen);

  }
};

/*!
 * The test function: we want to test the levenshtein algorithm
 */
void  test()
{
    levenshtein_distance_operator_v2 ld;

    /// strings to comapare
    std::string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
    std::string str2 = "igr-marfin-zeuthenersrt-15732|||2_ID|||traveller";    
    double wall01=tools::get_wall_time(); 
    double similarity = ld(str1,str2);
    wall01=tools::get_wall_time()-wall01;
    printf("similarity: %f,time: %f\n",1.-similarity,wall01);
}    



/**
 *  `../config.sh --bin`/g++   -fdiagnostics-color  -std=c++11 -fcilkplus -lcilkrts   `../config.sh --inc` -O3  -ftree-vectorize -ftree-vectorizer-verbose=2 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores -funsafe-math-optimizations  -Wall -Wextra -march=corei7 levenshtein_vectorization.cc -o levenshtein_vec  &>log
 *  ./warn_summary -s0 -pass log | les
 */
int main()
{

    test();
    return 0;

}
