
/** \page
!!! PragmaSimd
 * 
 * Example of the use of the pragma simd.
 */


#include <unistd.h>
//! <some worker function
void stall()
{
    usleep(1000 * 1000); // 100 * 1000 microseconds = .1 second
}



__attribute__ ((noinline))
__attribute__((vector))
__attribute__((aligned(16)))
float add_floats( float * __restrict a,float * __restrict  b, float * __restrict  c, unsigned int i)
{ 
    //float * x1 = (float *)__builtin_assume_aligned(a, 16);
    //float * x2 = (float *)__builtin_assume_aligned(b, 16);
    //float * x3 = (float *)__builtin_assume_aligned(c, 16);
    //stall();
    //float res =  x1[i] + x2[i] + x3[i] ;
    float res;
    res=  a[i] + b[i] + c[i] ;
    return res;
    //if (i>0) a[i-1] =  x1[i] + x2[i] + x3[i] ; 
    //return a[i-1];
}



__attribute__((vector))
__attribute__((aligned(16)))
float add_float( float a,float b, float c)
{    
    return a + b + c ; 
}



// let's use the local (gcc-4.9) versions. So use "" but not <>
//#include  "iostream"
#include <stdio.h> 
//#include <string.h>
//#include  "fstream"

#include  "vector"
#include  "cstring"
//#include <../cilk/include/c++/4.9.0/sstream>
//#include <sstream>
//#include  "../cilk/include/c++/4.9.0/sstream"
//#include  "locale"

#include <cilk/cilk.h>


#include "tools/cpu-time.hpp"
#include <chrono>

//#include <cilk/cilk.h>
//#include <cilk/cilk_api.h>

using namespace std;


const unsigned int n=300000;
int sum = 0;
void  test()
{

    float a[n]   __attribute__((aligned(16)));
    float b[n]   __attribute__((aligned(16)));
    float c[n]   __attribute__((aligned(16)));
    float a1[n]   __attribute__((aligned(16)));
//    a[:]=0.;b[:]=1.; c[:]=2.;a1[:]=0;

    
    double wall01;  
    wall01=tools::get_wall_time(); 
    
#pragma vector always
#pragma vector align  
#pragma ivdep
//#ifdef CILKFOR        
//    cilk_for(int i=0; i<n; i++){        
//#pragma simd noassert
#pragma SIMD noassert
    for (int i=0; i<n; i++) {        
//        a[i] = a[i] + b[i] + c[i];
        a[i]=add_floats(a,b,c,i);                         
    //    add_floats(a,b,c,i);                         
//#else        
//#endif        

    }

    
    #pragma vector always
    #pragma vector align 
    #pragma ivdep
    a1[:]=add_float(a1[:],b[:],c[:]);        
        
   

//    for (auto p:a) printf("%f\n",p);
//    for (auto p:a1) printf("%f\n",p);

    wall01=tools::get_wall_time()-wall01;
    printf("%f\n",wall01);
    

}    



// with vectorization
//  `../config.sh --bin`/g++  -std=c++11 -fcilkplus -lcilkrts   `../config.sh --inc` -O2  -ftree-vectorize -ftree-vectorizer-verbose=5 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores   vectorization_simd.cc -o vectorization 
// `../config.sh --bin`/g++  -std=c++11 -fcilkplus -lcilkrts   `../config.sh --inc` -O3  -ftree-vectorize -ftree-vectorizer-verbose=2 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores -funsafe-math-optimizations  -Wall -Wextra -march=corei7   vectorization_simd.cc -o vectorization
// without vectorization
//  `../config.sh --bin`/g++  -std=c++11 -fcilkplus -lcilkrts   `../config.sh --inc` vectorization_simd.cc -o vectorization 
int main(int argc, char *argv[])
{

    test();
    return 0;

}

