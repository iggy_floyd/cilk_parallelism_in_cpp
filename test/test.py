#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
/** \page 
!!!  Test suite of the project.
*  
*
*
*  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
*  All rights reserved.
*
*
*  Usage: %(scriptName)s
*
*/
/** \dir
!!!  Test suite of the project.
*
*/
'''


from subprocess import Popen,PIPE
import inspect
import sys


sys.path.append('.')



def test_cpp(file,incs="",args="",bin="g++"):

 cmd="python-config --cflags"
 cmd=cmd.split()
 (pyc,err) = Popen(cmd, stdout=PIPE).communicate()
 pyc=pyc.replace("-Wstrict-prototypes","");
 pyc=pyc.replace("\n",""); 
 cmd="%s -std=c++0x  -I. %s   -I/usr/local/include 	   %s -fpic %s.cpp -shared -lboost_python %s -o %s.so"%(bin,pyc,incs,file,args,file)
 print cmd
 return 
 cmd=cmd.split()
 (out,err) = Popen(cmd, stdout=PIPE).communicate()
 import importlib
 mymodule = importlib.import_module(file, package=file)
 
 return getattr(mymodule,"test")()

  




def hello():
   """  hello.cpp

   >>> hello()
   'hello, world'
   """
   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )
   
   
def levenshtein():
   r"""  levenshtein.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein()
   distance: 0 time:7.50779e-05
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   cmd_libs=""
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )


def fib():
   r"""  fib.cpp
         encode( '", \\, \\t, \\n' )
   >>> fib()   
   result: 102334155 time elapsed: 0.287057
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def loop():
   r"""  loop.cpp
         encode( '", \\, \\t, \\n' )
   >>> loop()   
   result: 611584889 time elapsed: 0.00391302
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def loop_lock():
   r"""  loop_lock.cpp
         encode( '", \\, \\t, \\n' )
   >>> loop_lock()   
   result: 611584889 time elapsed: 0.00391302
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def loop_reducer():
   r"""  loop_reducer.cpp
         encode( '", \\, \\t, \\n' )
   >>> loop_reducer()   
   result: 611584889 time elapsed: 0.00391302
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def string_reducer():
   r"""  string_reducer.cpp
         encode( '", \\, \\t, \\n' )
   >>> string_reducer()   
   result: abcdefghijklmnopqrstuvwxyz time elapsed: 0.704855
   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def array_notation():
   r"""  array_notation.cpp
         encode( '", \\, \\t, \\n' )
   >>> array_notation()   
   result: abcdefghijklmnopqrstuvwxyz time elapsed: 0.704855
   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

def pragma_simd():
   r"""  preama_simd.cpp
         encode( '", \\, \\t, \\n' )
   >>> pragma_simd()   
   result: abcdefghijklmnopqrstuvwxyz time elapsed: 0.704855
   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   (cmd_bin,err) = Popen(["../config.sh","--bin"], stdout=PIPE).communicate()
   cmd_bin+="/g++   -fcilkplus -lcilkrts "
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + 
   " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x",cmd_bin.replace("\n","")).split("\n")
   )

   


if __name__ == '__main__':


    print __doc__ % {'scriptName' : sys.argv[0]}
  
    import doctest
    
    doctest.testmod(verbose=True)
#    doctest.run_docstring_examples(hello, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein, globals(),verbose=True)
#    doctest.run_docstring_examples(fib, globals(),verbose=True)
#    doctest.run_docstring_examples(loop, globals(),verbose=True)
#    doctest.run_docstring_examples(loop_lock, globals(),verbose=True)
#    doctest.run_docstring_examples(loop_reducer, globals(),verbose=True)
#    doctest.run_docstring_examples(string_reducer, globals(),verbose=True)
#    doctest.run_docstring_examples(array_notation, globals(),verbose=True)
#    doctest.run_docstring_examples(pragma_simd, globals(),verbose=True)
    




    import os
    cmd='rm *.so'
    os.system(cmd)
