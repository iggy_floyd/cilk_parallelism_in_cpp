
/** \page
!!! PragmaSimd
 * 
 * Example of the use of the pragma simd.
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;


void add_floats(float *a, float *b, float *c, int n) { 
    int i; 
    #pragma simd 
    for (i=0; i<n; i++) { 
        a[i] = a[i] + b[i] + c[i] ; } 
} 



const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    float a[3],b[3],c[3];
    a[:]=0.;b[:]=1.; c[:]=2.;
    double wall01;  
    wall01=tools::get_wall_time(); 
    add_floats(a,b,c,3);    
    wall01=tools::get_wall_time()-wall01;
    for (auto p:a) std::cout<<p<<"\n";
    ss<<"result: "<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(pragma_simd)
{
    using namespace boost::python;
    def("test", test);

}
