
/** \page
!!! CilkFibonacci 
 * 
 * Example of application of the Wool technology for fibonacci algorithm
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include <cilk/cilk.h>
#include <cilk/cilk_api.h>



int fib(int n)
{
    if (n < 2)
        return n;
    int x = cilk_spawn fib(n-1);
    int y = fib(n-2);
    cilk_sync;
    return x + y;
}


const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    int m, n=40;
    double wall01;
    // Set the number of workers to be used
    __cilkrts_set_param("nworkers", tools::vprintf("%d",10).c_str());//! <We set up the parameters of the command line to initilaze properly  
    wall01=tools::get_wall_time(); 
    m = fib(n);        
    wall01=tools::get_wall_time()-wall01;
    ss<<"result: "<<m<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(fib)
{
    using namespace boost::python;
    def("test", test);

}
