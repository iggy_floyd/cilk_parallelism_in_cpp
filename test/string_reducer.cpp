
/** \page
!!! CilkStringReducer
 * 
 * Example of the application of the string reducer.
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include <cilk/cilk.h>
#include <cilk/reducer_string.h> //! <the reducer to carry the operations on strings
#include <string>


#include <unistd.h>
//! <some worker function
void stall()
{
    usleep(100 * 1000); // 100 * 1000 microseconds = .1 second
}




//! <the shared resource via the reducer
cilk::reducer<cilk::op_string> string_reducer;

//! <Some input parameters
int gs = 4; //grainsize

/*!
 * performs the loop with the cilk parallelism.
 */

void cilk_loop(cilk::reducer<cilk::op_string> &  s) {
     #pragma cilk grainsize = gs
    // Build the string
    cilk_for(char ch = 'a'; ch <= 'z'; ch++)
    {
        stall();
        *s += ch;
    }

    return;
}



const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    double wall01;  
    wall01=tools::get_wall_time(); 
    cilk_loop(string_reducer);    
    wall01=tools::get_wall_time()-wall01;    
    string str;
    string_reducer.move_out(str);
    ss<<"result: "<<str<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(string_reducer)
{
    using namespace boost::python;
    def("test", test);

}
