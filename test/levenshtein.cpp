/** \page
!!! Levenshtein algorithm
 * 
 * This example illustrates the use of the Levenshtein algorithm
 */ 
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"


#include "levenshtein-distance.hpp"

#include <chrono>
#include <thread>

using namespace std;



int calls = 0;




const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");

    double wall01,wall02,wall03,wall04;

    levenshtein_distance_operator_v2 ld;

    /// strings to comapare
    string str1 = "igor-marfin-zeuthenersrt-15732|||1_ID|||traveller";
    string str2 = "igor-marfin-zeuthenersrt-15732|||2_ID|||traveller";


    std::vector<std::pair<double,std::string>> results;
    wall01=tools::get_wall_time(); 
 
    results.push_back(std::make_pair(ld(str1,str2),str1));

    str2 = "igr-marfin-zeuthenersrt-15732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"remove one letter (o) from igor"));

    str2 = "igr-marin-zeuthenersrt-15732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"remove two  letters (o,f) from igor marfin"));

    str2 = "igur-marfin-zeothenersrt-15732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"exchange two letters (o,u) from igor and zeuthenersrt"));

    str2 = "tgor-marftn-euthenrsrt-15732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"remove one  letter (z) and  two i replaced by t "));

    str2 = "igor-marfin-zethenerst-1532|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"remove three  letters  (random)"));

    str2 = "igo-marfn-zuetheersrt-15732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"remove three  letters  (random)"));

    str2 = "igsor-marfin-zeutenersrt-1573|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"add one and remove 2  letters  (random)"));

    str2 = "igtr-7arfin-zeuthenersro-15m32|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"exchange 4  letters  (random)"));

    str2 = "5gur-narfin-zeothemersrt-1i732|||2_ID|||traveller";
    results.push_back(std::make_pair(ld(str1,str2),"exchange 6  letters  (random)"));


    results.push_back(std::make_pair(1./prepare_field_v3<std::string>(str1,-1).size(),"one letter cost"));

    wall02=tools::get_wall_time();
    ss<<"distance: "<<results[0].first<<" time:"<<wall02-wall01;
 
    for (auto& p : results) {
        std::cout<<"res = "<<p.first<<"\t"<<"description: "<<p.second<<"\n";
    }
    return ss.str();
}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(levenshtein)
{
using namespace boost::python;
def("test", test);

}



