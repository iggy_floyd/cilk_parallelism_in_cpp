
/** \page
!!! ArrayNotation
 * 
 * Example of the application of the string reducer.
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include <cilk/cilk.h>
#include <string>


//! <Some input parameters
const int array_size=10;





const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    int a[array_size]; 
    const char *loop_results[array_size]; 
    const char *an_results[array_size]; 
    // Initialize array using for loop 
    for (int i = 0; i < array_size; i++) a[i] = i;
    // Check for 5 using a for loop 
    for (int i = 0; i < array_size; i++) { 
        if (5 == a[i]) loop_results[i] = "Matched"; 
        else loop_results[i] = "Not Matched"; 
    }
    
    double wall01;  
    wall01=tools::get_wall_time(); 
    if (5 == a[:]) an_results[:] = "Matched";         
    else an_results[:] = "Not Matched"; 
    
    wall01=tools::get_wall_time()-wall01;    
    std::cout<<"loop version\n\n";
    for(auto p: loop_results) std::cout<<p<<"\n";
    std::cout<<"array version\n\n";
    for(auto p: an_results) std::cout<<p<<"\n";
    string str;    
    ss<<"result: "<<str<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(array_notation)
{
    using namespace boost::python;
    def("test", test);

}
