/** \page
!!! CilkLoopLock
 * 
 * Example of  a loop that permits loop iterations to run in parallel.
 * This is a the parallel version of a normal C/C++ for loop. cilk_for divides a loop into 
 * chunks containing one or more loop iterations. 
 * Once the loop is broken down, each chunk is executed on a specific thread of execution.  
 * 
 * Note that there is a race condition which should be solved.
 * One way to solve this problem is to use locks. 
 * Locks are synchronization mechanisms that prevent multiple threads from changing a variable concurrently. 
 * Thus, locks help to eliminate data races. 
 * Unfortunately, the use of locks decreases the speed of the evaluation of the loop.
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <pthread.h> //pthread library 

//! <Some input parameters
int n = 100000;
int gs = 4; //grainsize


//! <the shared resource with the lock
pthread_mutex_t m; //define the lock
int sum = 0;

/*!
 * performs the loop with the cilk parallelism.
 */

void cilk_loop(int &  sum) {
//    #pragma cilk grainsize = gs
    cilk_for (int i = 0; i <= n; i++) {
        pthread_mutex_lock(&m); //lock - prevents other threads from running this code
        sum += i;
        pthread_mutex_unlock(&m); //unlock - allows other threads to access this code 
    }
    
    return;
}





const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    double wall01;  
    wall01=tools::get_wall_time(); 
    cilk_loop(sum);
    wall01=tools::get_wall_time()-wall01;
    ss<<"result: "<<sum<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(loop_lock)
{
    using namespace boost::python;
    def("test", test);

}
