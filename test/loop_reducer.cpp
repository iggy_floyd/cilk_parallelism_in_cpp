
/** \page
!!! CilkLoopReducer
 * 
 * Example of  a loop that permits loop iterations to run in parallel.
 * This is a the parallel version of a normal C/C++ for loop. cilk_for divides a loop into 
 * chunks containing one or more loop iterations. 
 * Once the loop is broken down, each chunk is executed on a specific thread of execution.  
 * 
 * Note that there is a race condition which should be solved.
 * Another way is to use a reducer which is a variable that can be safely used by multiple threads running in parallel.
 * The runtime ensures that each thread has access to a private copy of the variable, 
 * eliminating the possibility of races without requiring locks. When the threads synchronize, 
 * the reducer copies are merged (or reduced) into a single variable. The runtime creates copies only 
 * when needed, minimizing overhead. 
 * There are multiple types of reducers: for mathematical operations, for strings, 
 * for determining minimum and maximum values of a list etc. For the full list of reducers check 
 * out the Intel® Cilk™ documentation. 
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h> //! <the reducer to carry the sum operation
#include <cilk/cilk_api.h>


//! <Some input parameters
int n = 100000;
int gs = 4; //grainsize


//! <the shared resource via the reducer

int sum = 0;
cilk::reducer_opadd<int> sum_reducer(sum);

/*!
 * performs the loop with the cilk parallelism.
 */

void cilk_loop(cilk::reducer_opadd<int> &  sum) {
//    #pragma cilk grainsize = gs
    cilk_for (int i = 0; i <= n; i++) {

        sum += i;
        
    }
    
    return;
}



const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    double wall01;  
    wall01=tools::get_wall_time(); 
    cilk_loop(sum_reducer);    
    wall01=tools::get_wall_time()-wall01;    
    sum_reducer.move_out(sum);
    ss<<"result: "<<sum<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(loop_reducer)
{
    using namespace boost::python;
    def("test", test);

}
