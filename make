export SCONSPATH=`pwd`/scons-build
export PYTHONPATH=$SCONSPATH/lib/python2.7/site-packages/
export LIBRARY_PATH=`pwd`/cilk/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=`pwd`/cilk/lib:$LD_LIBRARY_PATH 
make configuration # optional
make build
