#!/bin/bash
#

readoptions() {

local mode;


while [[ $# > 0 ]]
do

	local key="$1"
	shift

	case $key in
	--inc)
	    mode="inc"
            echo $mode
	    return 0
	    ;;
    	--libs)
    	    mode="libs"
            echo $mode
            return 0
            ;;
    	--bin)
    	    mode="bin"
            echo $mode
            return 0
            ;;
    	--libso)
    	    mode="libso"
            echo $mode
            return 0
            ;;
             *)
            echo "usage: $0 [--inc]"
            echo "       $0 [--libs]"
            echo "       $0 [--libso]"
            echo "       $0 [--bin]"
            return 1
            ;;
esac
done

echo "usage: $0 [--inc]"
echo "       $0 [--libs]"
echo "       $0 [--libso]"
echo "       $0 [--bin]"

return 1
}



configurepring() {


# additional compiler and linker options 
local additionallibs="-ldl  -lz -lm -lrt -lboost_thread-mt -lboost_system -lpthread"


local dir=`dirname  $0`;
[[ ${dir[0]} != "/" ]] && dir=`pwd`/$dir

local libso=$dir/libcilk_tool.so
dir=`find "$dir"   -iname "*.a"   | sort -h -r | grep -v "cilk/" | xargs -I {} echo {}`


local inc_echo
local libs_echo
local libs_file_echo

for i in $dir
 do
	local _dir_inc=`dirname $i | sed -e 's/lib$/include/g'`
	local _dir_libs=`dirname $i`
	local _file_libs=`basename $i | sed -e 's/lib/-l/g' | sed -e 's/\.a//g'`
	inc_echo=`echo "$inc_echo -I$_dir_inc"`
        libs_echo=`echo "$libs_echo -L$_dir_libs"`
        libs_file_echo=`echo "$libs_file_echo $_file_libs"`
 done


dir_script=`dirname $0`;
cilk_h=`find \`pwd\`/$dir_script -iname "*cilk_api.h" | grep  '\/cilk_api.h'`;
cilk_bin=`find \`pwd\`/$dir_script -iname "*g++" | grep  '\/g++'`;
cilk_bin=`dirname $cilk_bin`;
gcc_49_h=`find \`pwd\`/$dir_script -iname "*iostream" | grep  '\/iostream'`;
cilk_h=-I`dirname $cilk_h`;
gcc_49_h=-I`dirname $gcc_49_h`;
inc_echo="$inc_echo  $cilk_h $gcc_49_h";
libs_file_echo=" $_file_libs";
libs_echo=`echo "-Wl,--whole-archive  $libs_echo  $libs_file_echo -Wl,--no-whole-archive $additionallibs"`;



[[ $# == 1 ]] && [[ "$1" == "inc" ]] && echo "$inc_echo"  && return 0;
[[ $# == 1 ]] && [[ "$1" == "libs" ]] && echo "$libs_echo" && return 0;
[[ $# == 1 ]] && [[ "$1" == "libso" ]] && echo "$libso" && return 0;
[[ $# == 1 ]] && [[ "$1" == "bin" ]] && echo "$cilk_bin" && return 0;

return 1;
}




# the main program starts here

regime=`readoptions "$@"`
[[ $? > 0  ]] && echo "$regime" && exit $?
configurepring $regime

