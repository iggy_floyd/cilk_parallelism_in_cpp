/* 
 * File:   Documentation.h
 * Author: Igor Marfin <Unister Gmbh 2014 > igor.marfin@unister.de
 *
 * Created on September 3, 2015, 7:39 PM
 */

#ifndef DOCUMENTATION_H
#define	DOCUMENTATION_H

/*! \mainpage The documentation of the project
 *
 * This project is designed to test the cilk technology.
 * The basic introduction  and aim can be found in
 * <a href="README.html">README</a> file. 
 */


#endif	/* DOCUMENTATION_H */

