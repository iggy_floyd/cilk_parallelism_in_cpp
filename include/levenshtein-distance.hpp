
/** \page Levenshtein algorithm
!!!  Levenshtein algorithm
 * 
 *  This is the realization of the algorithm presented at
 * <a href="http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance">http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance</a>.
 *  
 *
 * 
 *  
 */


/** \dir
!!! 
 * 
 * The include directory contains the definitions and helper of 
 * classes/functions used in this project.
 */



#ifndef LEVENSHTEIN_H
#define	LEVENSHTEIN_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "tools/tools-text.hpp"


#include <chrono>
#include <thread>

/*!
 * Templated function which is the Levenshtein algorithm to calculate
 * the similarity between strings.
 */
template<class T>
unsigned int levenshtein_distance( const T &s11,  const T & s22) {

        std::vector<T> x1 = tools::split(s11, '_');
        std::vector<T> x2 = tools::split(s22, '_');
        T s1 =  x1[0];
        T s2 =  x2[0];

	const size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);

	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++) {
                    col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
                    prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
//                  std::this_thread::sleep_for(std::chrono::milliseconds(10)); // 1ms calculation
                }
		col.swap(prevCol);
	}
         
	return prevCol[len2];
}


/*!
 *  This function transforms the string in a specific way
 */
template<class T>
T prepare_field( const T &s11,int what_world_to_compare)
{

	 std::vector<T> x1 = tools::split(s11, '_');
  	 T x11 =  x1[0];
	 std::vector<T>  x111;

         int pos1;

 	 std::size_t found = x11.find(T("@"));
	 if (found!=T::npos) {
		 x111 = tools::split(x11,'@'); 
				}
         else  {
		 x111 = tools::split(x11,' ');

}	
	if (what_world_to_compare >=0 && what_world_to_compare< (int)x111.size()) pos1=what_world_to_compare;
        else  {
		if  (found!=T::npos) pos1 = 0;
		else 		pos1 = x111.size() -1;
	}

	std::transform(x111[pos1].begin(), x111[pos1].end(),x111[pos1].begin(), ::toupper);
	return  x111[pos1];
}

/*!
 *  This function transforms the string in a specific way
 */
template<class T>
T prepare_field_v2( const T &s11,int what_world_to_compare)
{

	 std::vector<T> x1 = tools::split(s11, '_');
  	 T x11 =  x1[0];
	 std::vector<T>  x111;

         int pos1;

		 x111 = tools::split(x11,' ');

	
	if (what_world_to_compare >=0 && what_world_to_compare< (int)x111.size()) pos1=what_world_to_compare;
        else  {
		pos1 = x111.size() -1;
	}

	std::transform(x111[pos1].begin(), x111[pos1].end(),x111[pos1].begin(), ::toupper);
	return  x111[pos1];
}


template<class T>
T prepare_field_v3( const T &s11,int what_world_to_compare)
{
                
//	 return tools::split(s11, '_')[0];
    // a new format: use '|||' delimiter             
         const T delim("|||");
         //return tools::tolower(tools::boostsplit<T>(s11, delim)[0]);
         return tools::boostsplit<T>(s11, delim)[0];
}


/*!
 *  This function transforms the string in a specific way
 */
template<class T>
unsigned int levenshtein_distance_v2( const T &s1, const T & s2) {
	const size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);

	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
			col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
								prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
		col.swap(prevCol);
	}
	return prevCol[len2];
}



/*!
 *  This functor represents the Levenshtein algorithm
 */
struct levenshtein_distance_operator {
  int operator()( const std::string &a,  const std::string &b) {
   /// see http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance
      return levenshtein_distance<std::string>(a, b);  
  }
    
};


/*!
 *  This functor represents the similarity algorithm
 */
struct levenshtein_distance_operator_v2 {
  double operator()( const std::string &a,  const std::string &b) {
     std::string str1 = prepare_field_v3<std::string>(a,-1);
     std::string str2 = prepare_field_v3<std::string>(b,-1);

     double distance = levenshtein_distance_v2<std::string>(str1, str2);  // http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance

     if (distance == 0 ) return  distance;
     double minlen = (double) std::min(str1.size(), str2.size());
     return       std::min(1.,distance/minlen);

  }
};


#endif
