/* 
 * File:   random_transformation_string.h
 * Author: debian
 *
 * Created on December 16, 2014, 12:12 AM
 */

#ifndef RANDOM_TRANSFORMATION_STRING_H
#define	RANDOM_TRANSFORMATION_STRING_H


#include <string>
#include <algorithm>
#include "tools-text.hpp"

namespace tools {

        /*!
         * returns the random string.
         * \param length defines the length of the returned string
         * 
         */
         std::string random_string( size_t length )
       {
        auto randchar = []() -> char
        {
        const char charset[] =
        "0123456789"
        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
  

/*!
 * The class is responsible for random generation of the strings.
 * 
 */          
class random_transformation_string {
    
public:
    random_transformation_string(std::string str,double dist):_str(tools::boostsplit<std::string>(str,std::string("|||"))[0]),_maxdist(dist) {
    
        // cost per one letter calculation 
        if (_str.empty())_cost = 0.;
        else     _cost = 1./_str.size();
  
        // calculation of possible schemas
        for (size_t exchange=0;exchange<_maxdist/(2*_cost);exchange++) {
           for (size_t remove=0;remove<_maxdist/(_cost);remove++) {
               for (size_t add=0;add<_maxdist/(_cost);add++) {
                   for (size_t replace=0;replace<_maxdist/(2*_cost);replace++) {
                        
                       if ( exchange*2*_cost + remove*_cost + add*_cost + replace*2*_cost < _maxdist)
                         schema_transformations.push_back(std::vector<size_t>({exchange,remove,add,replace})); 
                   }
               }           

           }
        }

        
        
    }
    std::string generate() {
        /*
        for (auto& p : schema_transformations) {
            
            std::cout<<"{";
            for (auto& p2 : p) std::cout<<p2<<",";
            std::cout<<"}\n";
                
}
        */
        
        //std::cout<<"cost = "<<_cost<<"\n";
        
        auto schema = schema_transformations[rand() % (schema_transformations.size() -1 ) ];
    
        
          /*
            std::cout<<"{";
            for (auto& p2 : schema) std::cout<<p2<<",";
            std::cout<<"}\n";
                */  

    
        
        
        auto exchange = schema[0];
        auto remove = schema[1];
        auto add = schema[2];
        auto replace = schema[3];
        
        auto total = exchange +remove +add +replace;
        while (total == 0 ) { 
            
             schema = schema_transformations[rand() % (schema_transformations.size() -1 ) ];
             exchange = schema[0];
             remove = schema[1];
             add = schema[2];
             replace = schema[3];
             total = exchange +remove +add +replace;
        
        }
        
        
        std::string result = _str;
        
        //do exchange
        for (size_t i=0;i<exchange;i++) {
//            std::cout<<"swapping\n";
            std::swap(result[rand() % (result.size()-1)],result[rand() % (result.size()-1)]);
//            std::cout<<result<<"\n";
            
            
        }
            
        // do remove
         for (size_t i=0;i<remove;i++) {
  //          std::cout<<"remove\n";
             result.erase (std::remove(result.begin(), result.end(), result[rand() % (result.size()-1)]), result.end());
//             std::cout<<result<<"\n";
         }
        // do add
        for (size_t i=0;i<add;i++) {
//            std::cout<<"add\n";
            result.insert(rand() % (result.size()-1),random_string(1));
//            std::cout<<result<<"\n";
        }
        // do replace
        for (size_t i=0;i<replace;i++) {
//              std::cout<<"replace\n";
            result.replace(rand() % (result.size()-1),1,random_string(1));
//              std::cout<<result<<"\n";
        }
        
        return result;
    }
    
    
    
    
private:
    std::string _str;
    double _cost;
    std::vector<std::vector<size_t> > schema_transformations;
    double _maxdist;
};

}
#endif	/* RANDOM_TRANSFORMATION_STRING_H */

