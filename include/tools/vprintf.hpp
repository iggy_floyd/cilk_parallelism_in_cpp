/* 
 * File:   vprintf.hpp
 * Author: debian
 *
 * Created on October 28, 2014, 3:10 PM
 */

#ifndef VPRINTF_HPP
#define	VPRINTF_HPP


#include <string>
#include <stdio.h>
#include <stdarg.h>

namespace tools {
    
    std::string vprintf ( const char *msg, ... )
    {
	va_list argList;
	char buffer[1024] = { 0 };
	va_start(argList, msg);
	vsprintf(buffer, msg, argList);
	va_end(argList);

    return std::string(buffer);

    }
    
    
}

#endif	/* VPRINTF_HPP */

