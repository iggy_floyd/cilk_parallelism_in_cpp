#ifndef TOOLS_TEXT_HPP
#define TOOLS_TEXT_HPP

#include <boost/algorithm/string/iter_find.hpp>
#include <boost/algorithm/string/finder.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>

namespace tools {
	template<class T>
	std::vector<T> &split(const T &s, char delim, std::vector<T> &elems) {
			    std::stringstream ss(s);
			    std::string item;
			    while (std::getline(ss, item, delim)) {
			        	elems.push_back(item);
			    }			
			    return elems;
			}


	template<class T>
	std::vector<T> split( const T &s, char delim) {
		    std::vector<T> elems;
		    split(s, delim, elems);
		    return elems;
	}


// works with strings	
	template<class T>
	T& rtrim(T& value) {
	  size_t last = value.find_last_not_of(" \n\r\t");
	  if (last != T::npos) {
	    value.erase(last+1);
	  }
	  return value;
	}


	template<class T>
	T tolower(T value) {
	  std::locale l("de_DE.UTF-8");
	   boost::algorithm::to_lower_copy(value,l);
	}


        template<class T>
	T& rtrimn(T& value) {
	  size_t last = value.find("\n");
	  if (last != T::npos) {
	    value.erase(last);
	  }
	  return value;
	}

        template<class T>
	T& add(T& value,const T  tobefound,const T tobeadded) {
	  size_t last = value.find(tobefound);
	  if (last != T::npos) {
              value.replace(last, tobefound.size(), tobefound+tobeadded);
	  }
	  return value;
	}

        

   template<class T>     
   std::vector<T> boostsplit( const T &s, const T delim) {
       std::vector<T> elems;
     boost::algorithm::iter_split(elems, s, boost::algorithm::first_finder(delim)); 
     return  elems;
   }
        
}
#endif
