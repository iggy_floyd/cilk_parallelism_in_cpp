

# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project




all: configuration doc build 


# to check that the system has all needed components
configuration: configure
	-@ mkdir src 2>/dev/null; mkdir include 2>/dev/null; mkdir lib 2> /dev/null;
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki 
	-@ mkdir doc 2>/dev/null
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc/{}.html; rm -r {}" | sh
	-@ doxys_1_15_0_linux32_bin/doxys


test: 
	@ echo "Test suites:"
	@ cd test; ./test.py



run: 
	@echo "Not implemented"


build: 
	@./build.sh



# to clean all temporary stuff
clean:  
	-@rm -r config.log autom4te.cache
	-@rm -r doc
	-@rm lib/* 2> /dev/null
	-@rm  `./config.sh --libso` 2> /dev/null
	-@ cd test; find ./ -executable -type f | awk '!/test\.py/ && !/README\.page/ && !/warn_summary/' | xargs -I {} rm {}
	
serve:
	-@firefox localhost:8010 & 
	-@firefox localhost:8010/README.html &
	-@php -S localhost:8010 -t doc


.PHONY: configuration clean all doc run test clean serve test
